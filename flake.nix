{
  description = "Alerts when a streamer is live";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    poetry2nix = {
      url = "github:nix-community/poetry2nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
  };

  outputs = { self, nixpkgs, poetry2nix, ... }:
    let
      systems = nixpkgs.lib.systems.flakeExposed;
      forEachSystem = nixpkgs.lib.genAttrs systems;
      pkgsFor = nixpkgs.lib.genAttrs systems (system: import nixpkgs {
        overlays = [ poetry2nix.overlays.default ];
        inherit system;
      });

    in
    {
      packages = forEachSystem (system: {
        stream-alert-bot = pkgsFor.${system}.callPackage ./package.nix { };
        default = self.outputs.packages.${system}.stream-alert-bot;
      });
      apps = forEachSystem (system: {
        stream-alert-bot = { type = "app"; program = "${nixpkgs.lib.getExe self.outputs.packages.${system}.stream-alert-bot}"; };
        default = self.outputs.apps.${system}.stream-alert-bot;
      });
      devShells = forEachSystem (system:
        let
          pkgs = pkgsFor.${system};
          inherit (pkgs) mkShell gnumake poetry;
          inherit (pkgs.python3Packages) python-lsp-server;
          devEnv = pkgs.poetry2nix.mkPoetryEnv { projectDir = (pkgsFor.${system}.lib.cleanSource ./.); };

        in
        {
          default = mkShell { buildInputs = [ devEnv gnumake poetry python-lsp-server ]; };
        });
      overlays.default = final: prev: { inherit (self.packages.${final.system}) stream-alert-bot; };
      formatter = forEachSystem (system: pkgsFor.${system}.nixpkgs-fmt);
      hmModule = import ./nix/modules/home-manager;
    };
}
