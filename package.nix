{ lib
, poetry2nix
}:

poetry2nix.mkPoetryApplication {
  projectDir = lib.cleanSource ./.;
  meta = {
    description = "Alerts when a streamer is live";
    homepage = "https://codeberg.org/wolfangaukang/stream-alert-bot";
    license = lib.licenses.gpl3Only;
    mainProgram = "stream_alert_bot";
    maintainers = with lib.maintainers; [ wolfangaukang ];
  };
}
